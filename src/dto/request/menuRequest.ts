import { SubmenuRequest } from "./submenuRequest"

export class MenuRequest {
    idMenu!: number
    nombre!: string
    color!: string
    icono!: string
    estado!: number
    submenus!: SubmenuRequest[]
}