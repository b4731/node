import { ServicioEmpresa } from '../../entidades/ServicioEmpresa';
export class ReservacionRequest {
    idReservacion!: number
    fechaReservacion!: string
    idUsuario!: number
    tiempoTranscurrido!: number
    direccion!:string
    listCarrito!: ServicioEmpresa[]
}