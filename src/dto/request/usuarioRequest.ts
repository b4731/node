export class UsuarioRequest {
    idUsuario!: number
    usuario!: string
    foto!:string
    archivoDni!:string
    clave!: string
    rol!: number
    rolCodigo!:string
    nombre!: string
    apellido!: string
    correo!: string
    telefono!: string
    tipo!: string
    dni!: string
    estado!: number
    fechaNacimiento!: string
    idPersona!: number
}