import { MenuRequest } from "./menuRequest"

export class SubmenuRequest {
    idSubmenu!: number
    nombre!: string
    icono!: string
    color!: string
    url!: string
    estado!: number
    menu!: MenuRequest

}