
import typeorm, {getConnection, getRepository} from "typeorm"
import { Persona } from '../entidades/Persona';
import { Rol } from '../entidades/Rol';


export class RolModel {
    async registrar(rol: Rol) {
        let result: Rol = await getRepository(Rol).save(rol)
        if (result) {
            return result
        } else {
            throw new Error("no se pudo registrar")
        }

    }
    async eliminar(id: number): Promise<typeorm.DeleteResult> {

        let eliminarPersona = await getRepository(Persona).delete({ id: id })
        if (eliminarPersona.affected) {

            return eliminarPersona

        } else {
            throw new Error("no se pudo eliminar la informacion")
        }
    }

    async listar() {
        let rolRepository = getRepository(Rol);
        console.log(rolRepository)
        let lista: Array<Rol> = await rolRepository.find({ order: { idRol: "ASC" } })
        return lista;
    }

    async buscarxCodigo(rolCodigo:string){
        let connection=getConnection()
        let queryRunner=await connection.createQueryRunner()
        await queryRunner.connect()
        let result=await queryRunner.query("select idRol from rol where codigo=?",[rolCodigo])

        let response=result[0];
        if (response.idRol!=undefined){
            return response
        }else{
            throw new Error("no se encontro rol")
        }
    }

}