import { EmpresaResponse } from '../dto/response/empresaResponse';
import typeorm, { getRepository } from "typeorm"
import { Empresa } from '../entidades/Empresa';
import { Propietario } from '../entidades/Propietario';
import { PropietarioResponse } from '../dto/response/propietarioResponse';
import { PersonaResponse } from '../dto/response/personaResponse';


export class EmpresaModel {
    async registrar(empresa: Empresa): Promise<EmpresaResponse> {

        let resultEmpresa: Empresa = await getRepository(Empresa)
            .query("call sp_empresas_crud(?,null,?,?,?,?,?,null,?)", [5, empresa.ruc,
                empresa.razonSocial, empresa.direccion, empresa.telefono, empresa.correo, empresa.propietario.id])
        console.log(resultEmpresa)
        if (resultEmpresa) {
            let empresaResponse = new EmpresaResponse();
            empresaResponse.IdEmpresa = resultEmpresa.id;
            empresaResponse.RazonSocial = resultEmpresa.razonSocial;
            empresaResponse.Ruc = resultEmpresa.ruc;
            empresaResponse.Direccion = resultEmpresa.direccion;
            empresaResponse.Correo = resultEmpresa.correo;
            empresaResponse.Telefono = resultEmpresa.telefono;
            return empresaResponse
        } else {
            throw new Error("no se pudo registrar empresa")
        }

    }
    async eliminar(id: number) {
        let result = await getRepository(Empresa).query("call sp_empresas_crud(?,?,null,null,null,null,null,null,null)", [4, id])
        return result
    }

    async buscarId(id: number) {
        let busqueda = await getRepository(Empresa).query("call sp_empresas_crud(?,?,null,null,null,null,null,null,null)", [3, id])
        console.log(busqueda)
        return busqueda[0]

    }

    async listarEmpresas(): Promise<Array<EmpresaResponse>> {
        let lista: Array<Empresa> = await getRepository(Empresa).query("call sp_empresas_crud(1,null,null,null,null,null,null,null,null)")
        //@ts-ignore
        let result = lista[0].map((empresa: Empresa, index) => {
            //@ts-ignore
          //  console.log(empresa)
          //  console.log("asas")
            let response = new EmpresaResponse()
            let propietario = new PropietarioResponse()
            response.Index = index + 1
            response.IdEmpresa = empresa.id;
            response.RazonSocial = empresa.razonSocial;
            response.Ruc = empresa.ruc;
            response.Direccion = empresa.direccion;
            response.Telefono = empresa.telefono;
            response.Correo = empresa.correo;
            //@ts-ignore
            propietario.IdPropietario = empresa.propietarioId
            //@ts-ignore
            propietario.Nombre = empresa.nombre
            //@ts-ignore
            propietario.Apellido = empresa.apellido
            response.Propietario = propietario
         //   console.log(response)
            return response;
        })
        return result;
    }

    async bucarRuc(ruc: string) {
        let result = await getRepository(Empresa).query("call sp_empresas_crud(?,null,?,null,null,null,null,null,null)", [2, ruc])
        console.log(result[0].length)

        return result[0]

    }

    async actualizar(empresa: Empresa, id: number) {
        console.log(empresa)
        let resultPersona = await getRepository(Empresa)
            .query("call sp_empresas_crud(?,?,?,?,?,?,?,null,?)", [6, id, empresa.ruc,
                empresa.razonSocial, empresa.direccion, empresa.telefono, empresa.correo, empresa.propietario.id])
        console.log(resultPersona)
        if (resultPersona) {
            return resultPersona
        } else {
            throw new Error("no se pudo actualizar empresa")
        }
    }

    async listarServiciosxEmpresas(id: number) {
        let detalle = await getRepository(Empresa).find({ where: { id }, relations: ["empresaServicio", "empresaServicio.servicio"] })
        return detalle
    }
}