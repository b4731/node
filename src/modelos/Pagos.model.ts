import { Pago } from '../entidades/Pago';
import { getRepository } from 'typeorm';
import { Reservacion } from '../entidades/Reservacion';
export class PagoModel {

    async registrar(pago: Pago) {
        let result = await getRepository(Pago).save(pago)
        let idReservacion = pago.reservacion.idReservacion;
        console.log(idReservacion)
        let actualizarEstadoReservacion = await getRepository(Reservacion)
            .query("update reservacion set estado=? where idReservacion=?",
                [2,idReservacion])
        console.log(actualizarEstadoReservacion)
        return result;
    }
}