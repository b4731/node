
import typeorm, { getConnection, getRepository } from "typeorm"
import { Manuales } from "../entidades/Manuales";
import { Persona } from '../entidades/Persona';
import { Rol } from '../entidades/Rol';


export class ManualesModel {
    async registrar(manual: Manuales) {
        let result: Manuales = await getRepository(Manuales).save(manual)
        if (result) {
            return result
        } else {
            throw new Error("no se pudo registrar")
        }

    }

    async listar() {
        let result = await getRepository(Manuales).find()
        return result
    }

    async eliminar(id: number) {
        let result = await getRepository(Manuales).delete(id)
        return result
    }

    async desactivar(id: number) {
        let findManual = await getRepository(Manuales).findOne(id)
        if (findManual != undefined) {
            findManual.status = 0
        }

        let result = await getRepository(Manuales).update(id, { ...findManual })
        return result
    }

}