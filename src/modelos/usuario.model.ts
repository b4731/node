import typeorm, { getConnection, getRepository, UpdateResult } from "typeorm"
import { UsuarioResponse } from "../dto/response/usuarioResponse";
import { Persona } from '../entidades/Persona';
import { Usuario } from "../entidades/Usuario";
import { UsuarioRepository } from "../repositorio/UsuarioRepository";
import { Cliente } from '../entidades/Cliente';
import { query } from "express";

export class UsuarioModel implements UsuarioRepository {

    async buscar(usuario: Usuario): Promise<Boolean> {
        let resultData = await getRepository(Usuario)
            .query("call sp_usuario_crud(3,null,?,?)",
                [usuario.usuario, usuario.clave])
        let result = resultData[0]
        console.log('RESULTADO---', result)
        if (result[0].rol != undefined) {
            //@ts-ignore
            result[0].rol = JSON.parse(result[0].rol)
            return true
        } else {
            return false
        }
    }

    async verificarCredenciales(usuario: Usuario): Promise<Object> {
        let result: Usuario | undefined = await getRepository(Usuario)
            .findOne({ where: { usuario: usuario.usuario, clave: usuario.clave }, relations: ["persona", "rol"] })
        console.log(result)
        if (result != undefined) {

            //@ts-ignore
            result.rol = result.rol.idRol
            return { verificado: true, usuario: result }
        } else {
            return { verificado: false, usuario: null }
        }
    }

    async registrar(usuario: Usuario): Promise<UsuarioResponse> {
        let connection = getConnection();
        let queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            await queryRunner.commitTransaction();
            let result = queryRunner.query("insert into usuario( usuario, clave, rolIdRol, personaId) values (?,?,?,?)"
                , [usuario.usuario, usuario.clave, usuario.rol.idRol, usuario.persona.id])
            console.log(result)
            return result
        } catch (e) {
            await queryRunner.rollbackTransaction();
            throw new Error("no se pudo registrar")
        }

        /* let result: Usuario = await getRepository(Usuario).save(usuario)
         if (result) {
             let usuarioResponse = new UsuarioResponse();
             usuarioResponse.IdUsuario = result.idUsuario;
             usuarioResponse.Usuario = result.usuario;
             usuarioResponse.Clave = result.clave;
             usuarioResponse.Rol = result.rol.idRol;
             if (usuarioResponse.Persona !== null)
                 usuarioResponse.Persona = result.persona
             return usuarioResponse
         } else {
             throw new Error("no se pudo registrar")
         }*/

    }

    async eliminar(id: number): Promise<typeorm.DeleteResult> {
        let result = await getRepository(Usuario)
            .query("delete from usuario u where u.idUsuario=?",
                [id])
        console.log(result)
        return result
    }

    async listar(): Promise<Array<UsuarioResponse>> {
        let lista: Array<Usuario> = await getRepository(Usuario)
            .query("call sp_usuario_crud(1,null,null,null)")

        console.log("----------------")
        //@ts-ignore
        //console.log(lista[0])
        //console.log("----------------")
        let result = lista[0].map((usuario: Usuario, index) => {

            let response = new UsuarioResponse()
            //@ts-ignore
            usuario.rol = JSON.parse(usuario.rol)
            // @ts-ignore
            usuario.persona = JSON.parse(usuario.persona)
            response.Index = index + 1
            response.IdUsuario = usuario.idUsuario
            response.Usuario = usuario.usuario
            response.Clave = usuario.clave
            response.Rol = usuario.rol.idRol
            response.Persona = usuario.persona
            return response;
        })
        return result;
    }

    async crearUsuario(persona: Persona, usuario: Usuario): Promise<UsuarioResponse> {
        let resultPersona: Persona = await getRepository(Persona).save(persona)
        if (resultPersona.id !== undefined) {
            let cliente = new Cliente();
            cliente.persona = resultPersona
            let clienteResult = await getRepository(Cliente).save(cliente)
            usuario.persona = resultPersona
            let result: Usuario = await getRepository(Usuario).save(usuario)
            console.log(result)
            if (result.idUsuario !== undefined) {
                let usuarioResponse = new UsuarioResponse();
                usuarioResponse.IdUsuario = result.idUsuario;
                usuarioResponse.Usuario = result.usuario;
                usuarioResponse.Clave = result.clave;
                usuarioResponse.Rol = result.rol.idRol;
                usuarioResponse.Persona = result.persona;
                return usuarioResponse
            } else {
                let eliminar = await getRepository(Usuario).delete({ idUsuario: resultPersona.id })
                throw new Error("no se pudo registrar Usuario")
            }
        } else {
            throw new Error("no se pudo registrar Persona")
        }
    }

    async buscarDni(dni: string) {
        let buscar = await getRepository(Usuario)
            .query("select usuario.*,persona.* from usuario " +
                "right join persona on usuario.personaid = persona.id " +
                "where persona.dni = " + dni
            )


        console.log(buscar)
        if (buscar !== undefined) {
            return buscar
        } else {
            throw new Error("no se encontro información")
        }
    }

    async usuarioBuscarId(id: number) {
        console.log("----->" + id + ">>>>>>>>>>>>>>>><");
        let resultData = await getRepository(Usuario)
            .query("call sp_usuario_crud(2,?,null,null)",
                [id])
        console.log(resultData)
        let result: Usuario = resultData[0][0];
        if (result) {
            //@ts-ignore
            result.rol = JSON.parse(result.rol)
            //@ts-ignore
            result.persona = JSON.parse(result.persona)
            let usuarioResponse = new UsuarioResponse();
            usuarioResponse.IdUsuario = result.idUsuario;
            usuarioResponse.Usuario = result.usuario;
            usuarioResponse.Clave = result.clave;
            usuarioResponse.Rol = result.rol.idRol;
            if (usuarioResponse.Persona !== null)
                usuarioResponse.Persona = result.persona
            return usuarioResponse
        } else {
            throw new Error("no se encontro informacion")
        }
    }

    async actualizarUsuario(usuario: Usuario, idUsuario: number) {
       let find= await getRepository(Usuario).findOne({ where: { idUsuario: idUsuario }})
        console.log('usuario foro',usuario.foto)
        if (usuario.foto===undefined){
        // @ts-ignore
            usuario.foto=find.foto
        }
        let resultUsuarios: UpdateResult = await getRepository(Usuario).update(idUsuario, usuario)

        const findUsuario = await getRepository(Usuario).findOne({ where: { idUsuario: idUsuario }, relations: ["persona", "rol"] })

        if (findUsuario != undefined) {
            let personaFind = await getRepository(Persona).findOne({ where: { id: findUsuario.persona.id } })
            if (personaFind != undefined) {
                switch (findUsuario.rol.idRol) {
                    //cuando el rol es cliente
                    case 2: {
                        personaFind.tipo = "Cliente"
                        console.log(personaFind)
                        await getRepository(Persona).update({ id: findUsuario.persona.id }, personaFind)
                        break
                    }
                    //cuando el rol es empleado
                    case 3: {
                        personaFind.tipo = "Empleado"
                        await getRepository(Persona).update({ id: findUsuario.persona.id }, personaFind)
                        break
                    }
                }

            }

        }



        if (resultUsuarios) {
            return resultUsuarios;
        } else {
            throw new Error("No se pudo actualizar Usuario")
        }

    }

    async buscarUsuarioDetalle(id: number) {
        return await getRepository(Usuario).findOne(id, { relations: ["persona"] })
    }


}
