import { EmpleadoResponse } from '../dto/response/empleadoResponse';
import typeorm, {DeleteResult, getConnection, getRepository, UpdateResult} from "typeorm"
import { Empleado } from '../entidades/Empleado';
import { Persona } from '../entidades/Persona';

export class EmpleadoModel {


    async listarEmpleados(): Promise<Array<EmpleadoResponse>> {
       // let lista: Array<Empleado> = await getRepository(Empleado).find({relations: ["persona", "empresa"]})
       // console.log(lista)
        let connection =await getConnection();
        let queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        let resultData: [] = await queryRunner.query("call sp_empleado_crud(4,null,null,null,null,null," +
            "null,null,null,null,null,null)")

        //let result = lista.map((empleado: Empleado, index) => {

        //@ts-ignore
        let result = resultData[0].map((item: any, index) => {
            //@ts-ignore
            let empleado=item

            let response = new EmpleadoResponse()
            response.Index = index + 1
            response.IdEmpleado = empleado.idEmpleado
            if (empleado.persona !== null) {
            //@ts-ignore
                empleado.persona=JSON.parse(empleado.persona)
                response.Nombre = empleado.persona.nombre
                response.Apellido = empleado.persona.apellido
                response.Dni = empleado.persona.dni
                response.FechaNacimiento = empleado.persona.fechaNacimiento
                response.Correo = empleado.persona.correo
                response.Celular = empleado.persona.celular
                response.Tipo = empleado.persona.tipo
                response.CreatedAt = empleado.persona.createdAt
                if (empleado.empresa !== null) {
                    //@ts-ignore
                    empleado.empresa=JSON.parse(empleado.empresa)
                    //@ts-ignore
                    response.IdEmpresa = empleado.empresa.id
                }
            }

            return response;
        })
        return result;
    }


    async registrarEmpleado(empleado: Empleado): Promise<EmpleadoResponse> {
        let connection = await getConnection();
        let queryRunner =await connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction()
        try{
            let resulUpdate= await queryRunner
                .query("call sp_empleado_crud(2,?,?,?,?,?,?,?,?,?,?,?)",
                    [empleado.persona.id,empleado.persona.id,empleado.persona.nombre,
                        empleado.persona.apellido,empleado.persona.dni,empleado.persona.fechaNacimiento,
                        empleado.persona.correo,empleado.persona.tipo,empleado.persona.celular,empleado.tipoEmpleado,
                    empleado.empresa.id])

            resulUpdate[0][0].persona=JSON.parse(resulUpdate[0][0].persona)
            await queryRunner.commitTransaction()
            return resulUpdate
        }catch (e) {
            console.log(e)
            await  queryRunner.rollbackTransaction()
            throw new Error("No se pudo registrar informacion");
        }
       /* let resultPersona = await getRepository(Persona).save(empleado.persona)
        console.log(empleado)
        if (resultPersona) {
            empleado.persona = resultPersona

            let resultEmpleado: Empleado = await getRepository(Empleado).save(empleado)

            console.log(resultEmpleado)
            if (resultEmpleado) {
                let responseEmpleado = new EmpleadoResponse()
                responseEmpleado.IdEmpleado = resultEmpleado.idEmpleado
                responseEmpleado.Dni = resultEmpleado.persona.dni
                responseEmpleado.Nombre = resultEmpleado.persona.nombre
                responseEmpleado.Apellido = resultEmpleado.persona.apellido
                responseEmpleado.FechaNacimiento = resultEmpleado.persona.fechaNacimiento
                responseEmpleado.Estado = resultEmpleado.persona.estado
                responseEmpleado.Correo = resultEmpleado.persona.correo
                responseEmpleado.Celular = resultEmpleado.persona.celular
                responseEmpleado.Tipo = resultEmpleado.persona.tipo
                return responseEmpleado;
            } else {
                console.log("no se pudo registrar empleado")
                let eliminarPersona = await getRepository(Persona).delete({ id: resultPersona.id })
                throw new Error("No se pudo registrar empleado")
            }
        } else {
            throw new Error("No se pudo registrar empleado")
        }*/
    }
    async eliminar(id: number) {
        let result = await getRepository(Empleado).query("delete from empleado where idEmpleado=?",[id])
        return result;

    }

    async buscar(id: number) {
       // let result: Empleado | undefined = await getRepository(Empleado).findOne({ idEmpleado: id }, { relations: ["persona", "empresa"] })
        let connection=await getConnection();
        let queryRunner=await connection.createQueryRunner();
        await  queryRunner.connect();
        let resultData:[]=await queryRunner.query("call sp_empleado_crud(1,null,?,null,null,null,null,null," +
            "null,null,null,null)",[id])
        //console.log(resultData)
        //@ts-ignore
        let result:Empleado=resultData[0][0]
        if (result !== undefined) {
            //@ts-ignore
            result.persona=JSON.parse(result.persona)
            //@ts-ignore
            result.empresa=JSON.parse((result.empresa))
            let responseEmpleado = new EmpleadoResponse()
            responseEmpleado.IdEmpleado = result.idEmpleado
            responseEmpleado.IdPersona = result.persona.id
            responseEmpleado.Dni = result.persona.dni
            responseEmpleado.Nombre = result.persona.nombre
            responseEmpleado.Apellido = result.persona.apellido
            responseEmpleado.FechaNacimiento = result.persona.fechaNacimiento
            responseEmpleado.Estado = result.persona.estado
            responseEmpleado.Correo = result.persona.correo
            responseEmpleado.Celular = result.persona.celular
            responseEmpleado.Tipo = result.persona.tipo
            responseEmpleado.tipoEmpleado=result.tipoEmpleado
            if (result.empresa !== null) {
                //@ts-ignore
                responseEmpleado.IdEmpresa = result.empresa.id
            }
            console.log(responseEmpleado)
            return responseEmpleado;
        } else {
            throw new Error("No se encontró empleado")
        }
    }

    async actualizarEmpleado(empleado: Empleado, idEmpleado: number) {

        // let lista: Array<Empleado> = await getRepository(Empleado).find({relations: ["persona", "empresa"]})
        // console.log(lista)
        let connection =await getConnection();
        let queryRunner =await connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction()
        try{
            let resulUpdate= await queryRunner
                .query("call sp_empleado_crud(5,?,?,?,?,?,?,?,?,?,?,?)",
                    [empleado.persona.id,idEmpleado,empleado.persona.nombre,
                    empleado.persona.apellido,empleado.persona.dni,empleado.persona.fechaNacimiento,
                    empleado.persona.correo,empleado.persona.tipo,empleado.persona.celular,empleado.tipoEmpleado,
                    empleado.empresa.id])
            console.log(resulUpdate)
            await queryRunner.commitTransaction()
            return resulUpdate
        }catch (e) {
            console.log(e)
           await  queryRunner.rollbackTransaction()
            throw new Error("No se pudo actualizar informacion");
        }


        /*let persona = new Persona();
        persona = empleado.persona;
        let resultPersona: UpdateResult = await getRepository(Persona).update({ id: persona.id }, persona)

        if (resultPersona) {
            let resultEmpleado: UpdateResult = await getRepository(Empleado).update(idEmpleado, empleado)
            console.log(resultEmpleado)
            if (resultEmpleado) {
                return resultEmpleado;
            } else {
                throw new Error("No se pudo ractualizar empleado")
            }
        } else {
            throw new Error("No se pudo actualizar empleado")
        }*/
    }
}