import {Reservacion} from '../entidades/Reservacion';
import {getRepository, getConnection} from 'typeorm';
import {Cliente} from '../entidades/Cliente';
import {Usuario} from '../entidades/Usuario';
import {DetalleReservacion} from "../entidades/DetalleReservacion";
import {Empleado} from "../entidades/Empleado";

export class ReservacionModel {

    async registrar(reservacion: Reservacion) {

        let newReservacion = new Reservacion()
        newReservacion.servicioEmpresa = reservacion.servicioEmpresa
        newReservacion.fechaReservacion = reservacion.fechaReservacion
        newReservacion.direccion = reservacion.direccion
        newReservacion.detalleReservaciones = reservacion.detalleReservaciones
        newReservacion.cliente = reservacion.cliente
        newReservacion.tiempoTranscurrido = reservacion.tiempoTranscurrido
        let resultReservacion: Array<DetalleReservacion> = []
        for (const detalle of newReservacion.detalleReservaciones) {
            let resultRegistro: DetalleReservacion = await getRepository(DetalleReservacion).save(detalle)
            resultReservacion.push(resultRegistro);
        }
        newReservacion.detalleReservaciones = resultReservacion
        let result = await getRepository(Reservacion).save(newReservacion)


        return result


    }

    async listar(idUsuario: number) {
        let usuario = await getRepository(Usuario).findOne({
            where: {
                idUsuario: idUsuario
            }, relations: ["persona"]
        });
        if (usuario != undefined) {
            let result = await getRepository(Reservacion)
                .find({
                    relations: ["servicioEmpresa", "detalleReservaciones", "cliente", "cliente.persona"],

                })
            //@ts-ignore
            let list = result.filter(item => item.cliente.persona.id === usuario.persona.id)

            return list
        } else {
            throw new Error("no se encontró información")
        }
    }

    async listarXempleados(idUsuario: number) {
        let persona = await getRepository(Usuario).findOne({
            where: {
                idUsuario: idUsuario
            }, relations: ["persona"]
        })

        if (persona != undefined) {
            let empleadoResult = await getRepository(Empleado).find({
                relations: ["persona", "detalleReservacion",
                    "detalleReservacion.reservacion",
                    "detalleReservacion.reservacion.servicioEmpresa",
                    "detalleReservacion.reservacion.servicioEmpresa.servicio",
                    "detalleReservacion.reservacion.cliente",
                    "detalleReservacion.reservacion.cliente.persona"], where: {
                    persona: {
                        id: persona.persona.id
                    }
                }
            })


            return empleadoResult

        } else {
            throw new Error("error en el servidor")
        }
    }

    buscar() {

    }

    actualizar() {

    }

    async buscarCliente(idUsuario: number) {
        let buscar = await getRepository(Usuario).findOne({where: {idUsuario: idUsuario}, relations: ["persona"]})
        if (buscar != undefined) {
            let personaId = buscar.persona.id
            let cliente = await getRepository(Cliente).findOne({
                relations: ["persona"],
                where: {persona: {id: personaId}}
            })
            if (cliente != undefined) {
                return cliente
            } else {
                throw new Error("cliente no encontrado")
            }

        } else {
            throw new Error("cliente no encontrado")
        }
    }

    async eliminar(id: number) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        let result = await runnerQuery.query("delete from reservacion where idReservacion=?", [id])
        return result

    }

    async completarServicio(idDetalleServicio: number) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        let result = await runnerQuery.query("update detalle_reservacion set estado=2  where id=?", [idDetalleServicio])
        return result
    }

    async filtrarPedidos(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        let result = await runnerQuery.query(`select se.*,
                                                     r.fechaReservacion,
                                                     dr.cantidad,
                                                     dr.monto,
                                                     e.razonSocial,
                                                     s.nombre      as nombre_servicio,
                                                     s.descripcion as servicio_descripcion,
                                                    s.lugarServicio,
       s.direccion
                                              from reservacion r
                                                       inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                       inner join reservacion_servicio_empresa_servicio_empresa rsese
                                                                  on r.idReservacion = rsese.reservacionIdReservacion
                                                       inner join servicio_empresa se
                                                                  on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                       inner join servicio s on se.servicioIdServicio = s.idServicio
                                                       inner join empresa e on se.empresaId = e.id
                                              WHERE date(fechaReservacion) BETWEEN ? AND ?`, [fechaIni, fechaFin])
        return result
    }

    async pedidos_diarios(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        let result = await runnerQuery.query(`select sum(dr.monto) as monto_dia,sum(dr.cantidad) as cantidad_dia,day(r.fechaReservacion) as dia
                                              from reservacion r
                                                  inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                  inner join reservacion_servicio_empresa_servicio_empresa rsese on r.idReservacion = rsese.reservacionIdReservacion
                                                  inner join servicio_empresa se on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                  inner join servicio s on se.servicioIdServicio = s.idServicio
                                                  inner join empresa e on se.empresaId = e.id
                                              WHERE date(fechaReservacion) BETWEEN ? AND ? group by dia`, [fechaIni, fechaFin])
        return result
    }

    async resumen_pedidos_diarios(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        return await runnerQuery.query(`select  e.razonSocial,sum(dr.cantidad) as cantidad, sum(dr.monto) as monto
                                              from reservacion r
                                                  inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                  inner join reservacion_servicio_empresa_servicio_empresa rsese on r.idReservacion = rsese.reservacionIdReservacion
                                                  inner join servicio_empresa se on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                  inner join servicio s on se.servicioIdServicio = s.idServicio
                                                  inner join empresa e on se.empresaId = e.id
                                        WHERE date(fechaReservacion) BETWEEN ? AND ? group by  e.razonSocial`, [fechaIni, fechaFin])

    }


    async filtrarVentas(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect();
        let query=`select se.*,
                                                     r.fechaReservacion,
                                                     dr.cantidad,
                                                     dr.monto,
                                                     e.razonSocial,
                                                     s.nombre      as nombre_servicio,
                                                     s.descripcion as servicio_descripcion,
                                                     s.lugarServicio,
                                                     s.direccion
                                              from reservacion r
                                                       inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                       inner join reservacion_servicio_empresa_servicio_empresa rsese
                                                                  on r.idReservacion = rsese.reservacionIdReservacion
                                                      inner join pago p on r.idReservacion = p.reservacionIdReservacion
                                                       inner join servicio_empresa se
                                                                  on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                       inner join servicio s on se.servicioIdServicio = s.idServicio
                                                       inner join empresa e on se.empresaId = e.id
                                              WHERE DATE_FORMAT(p.createdAt, '%m-%d-%Y') BETWEEN DATE_FORMAT(?, '%m-%d-%Y') AND DATE_FORMAT(?, '%m-%d-%Y')`
       // console.log(query)
        let result = await runnerQuery.query(query, [fechaIni, fechaFin])
        console.log(result)
        return result
    }

    async ventas_diarios(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        let result = await runnerQuery.query(`select sum(dr.monto) as monto_dia,sum(dr.cantidad) as cantidad_dia,day(r.fechaReservacion) as dia
                                              from reservacion r
                                                  inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                  inner join reservacion_servicio_empresa_servicio_empresa rsese on r.idReservacion = rsese.reservacionIdReservacion
                                                  join pago p on r.idReservacion = p.reservacionIdReservacion
                                                      inner join servicio_empresa se on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                  inner join servicio s on se.servicioIdServicio = s.idServicio
                                                  inner join empresa e on se.empresaId = e.id
                                              WHERE DATE_FORMAT(p.createdAt, '%m-%d-%Y') BETWEEN DATE_FORMAT(?, '%m-%d-%Y') AND DATE_FORMAT(?, '%m-%d-%Y') group by dia`, [fechaIni, fechaFin])
        return result
    }

    async resumen_ventas_diarios(fechaIni: any, fechaFin: any) {
        let connection = await getConnection()
        let runnerQuery = connection.createQueryRunner()
        await runnerQuery.connect()
        return await runnerQuery.query(`select  e.razonSocial,sum(dr.cantidad) as cantidad, sum(dr.monto) as monto
                                              from reservacion r
                                                  inner join detalle_reservacion dr on r.idReservacion = dr.reservacionIdReservacion
                                                  inner join reservacion_servicio_empresa_servicio_empresa rsese on r.idReservacion = rsese.reservacionIdReservacion
                                                  join pago p on r.idReservacion = p.reservacionIdReservacion
                                                  inner join servicio_empresa se on se.idServicioEmpresa = rsese.servicioEmpresaIdServicioEmpresa
                                                  inner join servicio s on se.servicioIdServicio = s.idServicio
                                                  inner join empresa e on se.empresaId = e.id
                                        WHERE DATE_FORMAT(p.createdAt, '%m-%d-%Y') BETWEEN DATE_FORMAT(?, '%m-%d-%Y') AND DATE_FORMAT(?, '%m-%d-%Y') group by  e.razonSocial`, [fechaIni, fechaFin])

    }
}
