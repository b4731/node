import { PropietarioResponse } from '../dto/response/propietarioResponse';
import typeorm, {getConnection, getRepository} from "typeorm"
import { Propietario } from '../entidades/Propietario';


export class PropietarioModel {


    async registrar(propietario: Propietario): Promise<PropietarioResponse> {
        let connection=await getConnection()
        let queryRunner=await connection.createQueryRunner()
        await queryRunner.connect()
        queryRunner.startTransaction()
        try {
            let resultRegistra=await queryRunner
                .query("call sp_propietario_crud(2,?,?,?,?,?,?,?,?,?,?)",
                    [null,null,propietario.persona.nombre,
                        propietario.persona.apellido,propietario.persona.dni,
                        propietario.persona.fechaNacimiento,propietario.persona.correo,
                        propietario.persona.tipo,
                        propietario.persona.celular,propietario.tipoComercial])
          //  console.log(">>>>>>>>>>>>><")
        // console.log(resultRegistra)
            await queryRunner.commitTransaction()
          //  console.log(">>>>>>>>>>>>><")
            let result: Propietario = resultRegistra[0][0]
            //console.log(result)
            //@ts-ignore
            result.persona=JSON.parse(result.persona)
                let propetarioResponse = new PropietarioResponse();
                propetarioResponse.IdPersona = result.persona.id;
                propetarioResponse.IdPropietario = result.id;
                propetarioResponse.Nombre = result.persona.nombre;
                propetarioResponse.Apellido = result.persona.apellido;
                propetarioResponse.Correo = result.persona.correo;
                propetarioResponse.Celular = result.persona.celular;
                propetarioResponse.Estado = result.persona.estado;
                propetarioResponse.FechaNacimiento = result.persona.fechaNacimiento;
                propetarioResponse.TipoComercial = result.tipoComercial;
                propetarioResponse.CreatedAt = result.persona.createdAt;
                return propetarioResponse;
        }catch (e) {
            console.log(e)
            queryRunner.rollbackTransaction()
            throw new Error("no se pudo registrar propietario")
        }
    }

    async eliminar(id: number): Promise<typeorm.DeleteResult> {
        let connection=getConnection();
        let queryRunner=connection.createQueryRunner();
        await  queryRunner.connect();
        let resultawait =await  queryRunner.query( "delete from propietario  where id=? ",
            [id])
        console.log(resultawait)
        return resultawait
    }

    async listarPropietarios(): Promise<Array<PropietarioResponse>> {
        //let lista: Array<Propietario> = await getRepository(Propietario).find({ order: { id: "ASC" }, relations: ["persona"] })
        let connection=getConnection();
        let queryRunner=connection.createQueryRunner();
        await  queryRunner.connect();

        try {
            let listaResult=await  queryRunner
                .query("select pro.id,pro.tipoComercial, json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,\n" +
                    "    'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona\n" +
                    "from  propietario pro\n" +
                    "    left join persona p on p.id = pro.personaId");

            let lista:Propietario[]=listaResult
            let result = lista.map((propietario: Propietario, index) => {
                //@ts-ignore
                propietario.persona=JSON.parse(propietario.persona)
                let propetarioResponse = new PropietarioResponse();
                propetarioResponse.Index = index + 1
                propetarioResponse.IdPersona = propietario.persona.id;
                propetarioResponse.IdPropietario = propietario.id;
                propetarioResponse.Nombre = propietario.persona.nombre;
                propetarioResponse.Apellido = propietario.persona.apellido;
                propetarioResponse.Correo = propietario.persona.correo;
                propetarioResponse.Dni = propietario.persona.dni;
                propetarioResponse.Tipo = propietario.persona.tipo;
                propetarioResponse.Celular = propietario.persona.celular;
                propetarioResponse.Estado = propietario.persona.estado;
                propetarioResponse.FechaNacimiento = propietario.persona.fechaNacimiento;
                propetarioResponse.TipoComercial = propietario.tipoComercial;
                propetarioResponse.CreatedAt = propietario.persona.createdAt;
                return propetarioResponse

            })
            return result;

        }catch (e) {

            throw new Error("no se pudo listas propietarios")
        }

    }

    async buscar(id: number) {
        let connection =getConnection()
        let queryRunner=connection.createQueryRunner();
        await queryRunner.connect();
        let resultData:[]=await queryRunner.query("select pro.id,pro.tipoComercial, json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,\n" +
            "    'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona\n" +
            "from  propietario pro\n" +
            "    left join persona p on p.id = pro.personaId " +
            "where pro.id=?",[id])


        //let result = getRepository(Propietario).findOne({ id }, { relations: ["persona"] })
        if (resultData.length>0) {
            //@ts-ignore
            let result=resultData[0]
            //@ts-ignore
            result.persona=JSON.parse(result.persona)
            return result
        } else {
            throw new Error("no se encontro información")
        }
    }

    async actualizar(propietario: Propietario, idPropietario: number) {
        let connection=await getConnection()
        let queryRunner=await connection.createQueryRunner()
        await queryRunner.connect()
        queryRunner.startTransaction()
        try {
            let resultUpdate=await queryRunner
                .query("call sp_propietario_crud(1,?,?,?,?,?,?,?,?,?,?)",
                    [propietario.persona.id,idPropietario,propietario.persona.nombre,
                    propietario.persona.apellido,propietario.persona.dni,
                        propietario.persona.fechaNacimiento,propietario.persona.correo,
                    propietario.persona.tipo,
                    propietario.persona.celular,propietario.tipoComercial])
            console.log(">>>>>>>>>>>>><")
            console.log(resultUpdate)
            await queryRunner.commitTransaction()
            console.log(">>>>>>>>>>>>><")
            return resultUpdate
        }catch (e) {
            console.log(e)
            queryRunner.rollbackTransaction()
            throw new Error("no se pudo actualizar persona")
        }

        /*let resultPersona = await getRepository(Persona)
            .update({ id: propietario.persona.id }, propietario.persona)
        if (resultPersona) {
            let resultPropietario = await getRepository(Propietario).update({ id: idPropietario }, propietario)
            if (resultPropietario) {
                return resultPropietario
            } else {
                throw new Error("no se pudo actualizar propietario")
            }
        } else {
            throw new Error("no se pudo actualizar persona")
        }*/

    }
}