import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Menu } from './Menu';
import { RolSubmenu } from './RolSubmenu';


@Entity()
export class Submenu {

    @PrimaryGeneratedColumn()
    idSubmenu!: number

    @Column()
    nombre!: string

    @Column({ nullable: true })
    icono!: string

    @Column({ nullable: true })
    color!: string

    @Column({ nullable: true })
    url!: string

    @Column({ default: 1 })
    estado!: number

    @ManyToOne(type => Menu, menu => menu.submenus)
    menu!: Menu

    @OneToMany(type => RolSubmenu, rolsubmenu => rolsubmenu.rolsubmenu)
    rolSubmenu!: RolSubmenu[]

}