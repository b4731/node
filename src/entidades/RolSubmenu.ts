import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Rol } from './Rol';
import { Submenu } from './Submenu';
@Entity()
export class RolSubmenu {

    @PrimaryGeneratedColumn()
    idRolSubmenu!: number

    @Column({ default: 1 })
    estado!: number
    @ManyToOne(type => Rol, rol => rol.rolSubmenu)
    rol!: Rol

    @ManyToOne(type => Submenu, submenu => submenu.rolSubmenu)
    rolsubmenu!: Submenu


}