import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Submenu } from './Submenu';

@Entity()
export class Menu {

    @PrimaryGeneratedColumn()
    idMenu!: number

    @Column()
    nombre!: string

    @Column({ nullable: true })
    color!: string

    @Column({ nullable: true })
    icono!: string

    @Column({ default: 1 })
    estado!: number

    @OneToMany(type => Submenu, submenu => submenu.menu)
    submenus!: Submenu[]
}