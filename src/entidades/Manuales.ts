import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Submenu } from './Submenu';

@Entity()
export class Manuales {

    @PrimaryGeneratedColumn()
    id!: number

    @Column()
    nombre!: string

    @Column()
    archivo!: string

    @Column({ default: 1 })
    status!: number

}