import {Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Reservacion} from "./Reservacion";
import {Empleado} from "./Empleado";

@Entity()
export class DetalleReservacion {

    @PrimaryGeneratedColumn()
    id!:number

    @Column({default:1})
    cantidad!:number

    @Column({type:"float"})
    monto!:string

    @Column({default:1})
    estado!:number

    @ManyToOne(type => Reservacion,reservacion=>reservacion.detalleReservaciones)
    reservacion!:Reservacion

    @ManyToOne(type => Empleado,empleado=>empleado.detalleReservacion)
    empleado!:Empleado

}