import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { RolSubmenu } from './RolSubmenu';
import { Usuario } from './Usuario';

@Entity()
export class Rol {

    @PrimaryGeneratedColumn()
    idRol!: number

    @Column()
    nombre!: string

    @Column({nullable:true})
    codigo!: string

    @Column({ default: 1 })
    estado!: number

    @OneToMany(type => RolSubmenu, rol => rol.rol)
    rolSubmenu!: RolSubmenu[]

    @OneToMany(type => Usuario, usuario => usuario.rol)
    usuarios!: Usuario[]


}