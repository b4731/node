import { ManyToOne, Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Reservacion } from './Reservacion';


@Entity()
export class Pago {
    @PrimaryGeneratedColumn()
    id!: number

    @Column()
    idTransaccion!: string

    @Column({ type: "float" })
    monto!: string

    @CreateDateColumn()
    createdAt!: string

    @ManyToOne(type => Reservacion, reservacion => reservacion.pago)
    reservacion!: Reservacion

}