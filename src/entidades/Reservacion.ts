import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { ServicioEmpresa } from './ServicioEmpresa';
import { Cliente } from './Cliente';
import { DetalleReservacion } from "./DetalleReservacion";
import { Pago } from './Pago';

@Entity()
export class Reservacion {

    @PrimaryGeneratedColumn()
    idReservacion!: number

    @Column({ default: 1 })
    estado!: number

    @Column()
    tiempoTranscurrido!: number

    @Column({ nullable: true })
    fechaReservacion!: string

    @Column({ nullable: true })
    direccion!: string

    @ManyToOne(type => Cliente, cliente => cliente.reservacion)
    cliente!: Cliente

    @ManyToMany(type => ServicioEmpresa)
    @JoinTable()
    servicioEmpresa!: ServicioEmpresa[]

    @OneToMany(type => DetalleReservacion,
        detalleReservacion => detalleReservacion.reservacion, { onDelete: "CASCADE" })
    detalleReservaciones!: DetalleReservacion[]

    @OneToMany(type => Pago, pago => pago.reservacion)
    pago!: Pago


}