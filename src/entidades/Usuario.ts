import typeorm, { OneToMany, ManyToOne, Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, Table, UpdateDateColumn } from "typeorm"
import { Persona } from './Persona';
import { Rol } from "./Rol";


@Entity()
export class Usuario {
    @PrimaryGeneratedColumn()
    idUsuario!: number

    @Column({ unique: true })
    usuario!: string

    @Column()
    clave!: string

    @Column()
    foto!: string

    @ManyToOne(type => Rol, rol => rol.usuarios)
    rol!: Rol

    @CreateDateColumn()
    createdAt!: string

    @UpdateDateColumn()
    updatedAt!: string

    @OneToOne(type => Persona)
    @JoinColumn()
    persona!: Persona;
}