import { PersonaModel } from '../modelos/Persona.model';
import { PersonaRequest } from '../dto/request/personaRequest';
import { Persona } from '../entidades/Persona';
import {UsuarioRequest} from "../dto/request/usuarioRequest";
import {Usuario} from "../entidades/Usuario";
import {Rol} from "../entidades/Rol";


export class PersonaControl {
    private model: PersonaModel
    constructor() {
        this.model = new PersonaModel()
    }
    async listarPersonas() {
        let result = await this.model.listarPersonas()
        return result;
    }

    async registrar(request: PersonaRequest) {
        let persona = new Persona()
        if (request.nombre != null) persona.nombre = request.nombre;
        if (request.apellido != null) persona.apellido = request.apellido;
        if (request.dni != null) persona.dni = request.dni;
        let result = await this.model.registrar(persona)
        return result;

    }

    async eliminar(id: number) {
        let result = await this.model.eliminar(id)
        return result;

    }

    async buscarDni(dni: string) {
        let result = await this.model.buscarDni(dni)
        return result;
    }

    async actualizar(id: number, personaRequest: PersonaRequest) {
        let persona = new Persona()
       persona.dni=personaRequest.dni
        persona.nombre=personaRequest.nombre
        persona.apellido=personaRequest.apellido
        persona.correo=personaRequest.correo
        persona.celular=personaRequest.celular
        persona.tipo=personaRequest.tipo
        persona.fechaNacimiento=personaRequest.fechaNacimiento
        persona.archivoDni=personaRequest.archivoDni
        let result = await this.model.actualizarPersona(persona, id)
        return result;
    }
}