
import jwt_token from "../middleware/jwt-token"
import { Rol } from '../entidades/Rol';
import { RolModel } from '../modelos/Rol.model';
import { RolRequest } from '../dto/request/rolRequest';

export class RolControl {
    private model: RolModel
    constructor() {
        this.model = new RolModel()
    }
    async listar() {
        let result = await this.model.listar()
        return result;
    }

    async registrar(request: RolRequest) {
        let rol = new Rol()
        if (request.nombre != null) rol.nombre = request.nombre;
        let result = await this.model.registrar(rol)
        return result;

    }



    async eliminar(id: number) {
        let result = await this.model.eliminar(id)
        return result;

    }


}