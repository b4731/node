import { Pago } from '../entidades/Pago';
import { PagoModel } from '../modelos/Pagos.model';
export class PagoControl {

    private model: PagoModel
    constructor() {
        this.model = new PagoModel();
    }
    async registrar(pago: Pago) {
        let result = await this.model.registrar(pago)
        return result;
    }
}