import { ReservacionModel } from '../modelos/Reservacion.model';
import { ReservacionRequest } from '../dto/request/reservacionRequest';
import { Reservacion } from '../entidades/Reservacion';
import { Cliente } from '../entidades/Cliente';
import { Usuario } from '../entidades/Usuario';
import { ServicioEmpresa } from '../entidades/ServicioEmpresa';
import { DetalleReservacion } from "../entidades/DetalleReservacion";
import { Empleado } from "../entidades/Empleado";



export class ReservacionControl {
    private model: ReservacionModel
    constructor() {
        this.model = new ReservacionModel()
    }
    async listar(idUsuario: number) {
        let result = await this.model.listar(idUsuario)
        return result;
    }

    async listarxEmpleado(idUsuario: number) {
        let result = await this.model.listarXempleados(idUsuario)
        return result
    }

    async registrar(request: ReservacionRequest) {

        try{
            let servicio = new Reservacion()
            if (request.idUsuario != undefined) {
                let cliente: Cliente = await this.model.buscarCliente(request.idUsuario)
                servicio.cliente = cliente
            }
            if (request.fechaReservacion != undefined) servicio.fechaReservacion = request.fechaReservacion
            if (request.listCarrito != undefined) {
                let servicios: ServicioEmpresa[] = []
                let listaRequest: ServicioEmpresa[] = request.listCarrito;
                let detalleReservacionList: Array<DetalleReservacion> = []
                servicios = listaRequest.map(item => {
                    let itemServicio = new ServicioEmpresa()
                    let detalleReservacion = new DetalleReservacion();
                    //@ts-ignore
                    detalleReservacion.cantidad = item.cantidad;
                    //@ts-ignore
                    detalleReservacion.monto = item.monto;
                    let empleado = new Empleado()
                    //@ts-ignore
                    empleado.idEmpleado = item.idEmpleado

                    detalleReservacion.empleado = empleado
                    detalleReservacionList.push(detalleReservacion);
                    itemServicio.idServicioEmpresa = item.idServicioEmpresa
                    return itemServicio

                })
                servicio.servicioEmpresa = servicios
                servicio.detalleReservaciones = detalleReservacionList


            }
            servicio.direccion=request.direccion
            servicio.tiempoTranscurrido=request.tiempoTranscurrido

            let result = await this.model.registrar(servicio)
            return result
        }catch (error) {
            console.log(error)
            throw new Error("Error en reservacion registrar");
        }
    }

    async eliminar(id: number) {
        let result = this.model.eliminar(id)
        return result
    }

    async completarServicio(idDetalleServicio: number) {
        let result = this.model.completarServicio(idDetalleServicio)
        return result
    }


}
