import {ReservacionModel} from "../modelos/Reservacion.model";


export class ReportesController {
    private model: ReservacionModel

    constructor() {
        this.model = new ReservacionModel()
    }

    async pedidos(fechaIni:any, fechaFin:any) {
        return await this.model.filtrarPedidos(fechaIni,fechaFin)
    }
    async pedidos_diarios(fechaIni:any, fechaFin:any) {
        return await this.model.pedidos_diarios(fechaIni,fechaFin)
    }
    async resumen_pedidos_diarios(fechaIni:any, fechaFin:any) {
        return await this.model.resumen_pedidos_diarios(fechaIni,fechaFin)
    }

    async ventas(fechaIni:any, fechaFin:any) {
        return await this.model.filtrarVentas(fechaIni,fechaFin)
    }
    async ventas_diarios(fechaIni:any, fechaFin:any) {
        return await this.model.ventas_diarios(fechaIni,fechaFin)
    }
    async resumen_ventas_diarios(fechaIni:any, fechaFin:any) {
        return await this.model.resumen_ventas_diarios(fechaIni,fechaFin)
    }
}
