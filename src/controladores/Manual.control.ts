import { Manuales } from '../entidades/Manuales';
import { Pago } from '../entidades/Pago';
import { ManualesModel } from '../modelos/Manuales.model';
import { PagoModel } from '../modelos/Pagos.model';
export class ManualControl {

    private model: ManualesModel
    constructor() {
        this.model = new ManualesModel();
    }
    async registrar(manuales: Manuales) {
        let result = await this.model.registrar(manuales)
        return result;
    }

    async listar() {
        return await this.model.listar();
    }


    async eliminar(id: number) {
        return await this.model.eliminar(id);
    }

    async desactivar(id: number) {
        return await this.model.desactivar(id);
    }

}