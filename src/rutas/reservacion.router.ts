import { Router, Request, Response } from "express"
import { ReservacionRequest } from '../dto/request/reservacionRequest';
import { ReservacionControl } from '../controladores/Reservacion.control';
const router = Router()
let control = new ReservacionControl()

router.post("/registrar", async (req: Request, res: Response) => {
    try {
        let reservacionRequest = new ReservacionRequest()
        reservacionRequest = req.body
        let result = await control.registrar(reservacionRequest)
        console.log(result)
        res.status(200).send(result)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})

router.get("/listar/:id", async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let idx = parseInt(id)

        let result = await control.listar(idx)
        console.log(result)
        res.status(200).send(result)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})


router.delete("/cancelar/:id", async (req: Request, res: Response) => {
    try {

        let { id } = req.params
        let idx = parseInt(id)
        let result = await control.eliminar(idx);
        res.status(200).send(result)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})

router.get("/listar/empleado/:id", async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let idUsuario = parseInt(id);

        let result = await control.listarxEmpleado(idUsuario)
        console.log(result)
        res.status(200).send(result)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})

router.get("/completar/servicio/:id", async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let idDetalleServicio = parseInt(id);

        let result = await control.completarServicio(idDetalleServicio)
        console.log(result)
        res.status(200).send(result)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})

export default router
