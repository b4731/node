import  { Router, Response, Request } from "express"
import verificarAutenticacion from "../middleware/autenticacion/auth"
import auth from '../middleware/jwt-token';
import { RolRequest } from '../dto/request/rolRequest';
import {ReportesController} from "../controladores/ReportesController";

let control = new ReportesController()
let router = Router();


router.get("/pedidos", function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
}, async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

     let result = await control.pedidos(fechaIni,fechaFin)
    res.status(200).json(result);
})


router.get("/pedidos_diarios", function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
},async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

    let result = await control.pedidos_diarios(fechaIni,fechaFin)
    console.log(result)
    res.status(200).json(result);
})

router.get("/resumen/pedidos_diarios",function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
}, async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

    let result = await control.resumen_pedidos_diarios(fechaIni,fechaFin)
    res.status(200).json(result);
})


// VENTAS

router.get("/ventas", function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
}, async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

    let result = await control.ventas(fechaIni,fechaFin)
    res.status(200).json(result);
})


router.get("/ventas_diarios", function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
},async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

    let result = await control.ventas_diarios(fechaIni,fechaFin)
    console.log(result)
    res.status(200).json(result);
})

router.get("/resumen/ventas_diarios",function (req,res,next){
    if (req.method == "OPTIONS") {
        res.status(200);
        res.send();
    }else{
        next();
    }
}, async (req: Request, res: Response) => {
    const {fechaIni,fechaFin}=req.query;

    let result = await control.resumen_ventas_diarios(fechaIni,fechaFin)
    res.status(200).json(result);
})


export default router
