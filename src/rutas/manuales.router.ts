import express, { Router, Response, Request } from "express"
import verificarAutenticacion from "../middleware/autenticacion/auth"
import auth from '../middleware/jwt-token';
import { RolControl } from '../controladores/Rol.control';
import { RolRequest } from '../dto/request/rolRequest';
import { Manuales } from "../entidades/Manuales";
import { ManualControl } from "../controladores/Manual.control";

let control = new ManualControl();
let router = Router();


router.post("/", async (req: Request, res: Response) => {

    let files = req.files?.file
    console.log(files)
    //@ts-ignore
    files.mv('./uploads/manuales/' + files.name);
    let resultUploads = {
        status: true,
        message: 'archivo subido',
        data: {
            //@ts-ignore
            name: files.name,
            //@ts-ignore
            mimetype: files.mimetype,
            //@ts-ignore
            size: files.size
        }
    }
    let query = req.query
    console.log(query)
    let data = new Manuales();
    //@ts-ignore
    data.archivo = 'uploads/manuales/' + files.name
    //@ts-ignore
    data.nombre = query.tipo
    let result = await control.registrar(data)
    res.status(200).json({ ...resultUploads, ...result });
})

router.get('/', async (req, res) => {
    console.log('listando')
    let result = await control.listar()
    res.status(200).json(result)
})

router.delete('/eliminar/:id', async (req, res) => {
    let { id } = req.params
    let intID = Number.parseInt(id)
    let result = await control.eliminar(intID)
    res.status(200).json(result)
})



router.put('/desactivar/:id', async (req, res) => {
    let { id } = req.params
    let intID = Number.parseInt(id)
    let result = await control.desactivar(intID)
    res.status(200).json(result)
})


export default router
