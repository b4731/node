import { Router, Request, Response } from "express"
import crypto from "crypto"
import squareConnect from "square-connect"
import Stripe from "stripe"
import { Pago } from '../entidades/Pago';
import { PagoControl } from '../controladores/Pago.control';
import { Reservacion } from "../entidades/Reservacion";
const stripe = new Stripe(
    'sk_test_51HSF05B8uMZyBz5LJzYn0l5LZ66miggKIAjNFmaT4Fjoe9ORUQt8tID0XMFFQMgD4pcIbiMnnrXOb65uKO2ln4uM00T4RnXczY',
    {
        apiVersion: '2020-08-27',
    });


const router = Router()

let control = new PagoControl()

router.post("/pagarServicio", async (req: Request, res: Response) => {

    var defaultClient = squareConnect.ApiClient.instance;
    // Set sandbox url
    defaultClient.basePath = 'https://connect.squareupsandbox.com';
    // Configure OAuth2 access token for authorization: oauth2
    var oauth2 = defaultClient.authentications['oauth2'];
    // Set sandbox access token
    oauth2.accessToken = "EAAAECF_HOYVaq1y7Fp6HxkADxy6aVF4xvDNG9Pk-6rYK4RiZh6pX31zT9Z_xErR";
    // Pass client to API
    var api = new squareConnect.LocationsApi();
    console.log(api)

    const request_params = req.body;

    const { monto, idReservacion } = req.body
    // length of idempotency_key should be less than 45
    const idempotency_key = crypto.randomBytes(22).toString("hex");
    console.log(idempotency_key)

    // Charge the customer's card
    const payments_api = new squareConnect.PaymentsApi();
    console.log(payments_api)
    const request_body = {
        source_id: request_params.nonce,
        amount_money: {
            amount: monto * 100, // $1.00 charge
            currency: "USD",
        },
        idempotency_key: idempotency_key,
    };

    try {
        //@ts-ignore
        const response = await payments_api.createPayment(request_body);
        let newPago = new Pago()
        console.log(response)
        //@ts-ignore
        newPago.idTransaccion = response.payment.id
        let reservacion = new Reservacion()
        reservacion.idReservacion = idReservacion
        newPago.reservacion = reservacion
        newPago.monto = monto
        let reesultRegistro = await control.registrar(newPago);
        console.log(reesultRegistro);
        res.status(200).send(
            reesultRegistro
        );

    } catch (error) {
        console.log(error)
        res.status(500).json({
            title: "Payment Failure",
            result: error,
        });
    }
})

router.post("/stripe/pago", async (req: Request, res: Response) => {
    console.log(req.body)
    const session = await stripe.checkout.sessions.create({
        payment_method_types: ["card"],
        line_items: [
            {
                price_data: {
                    currency: "usd",
                    product_data: {
                        name: "T-shirt",
                    },
                    unit_amount: 2000,
                },
                quantity: 1,
            },
        ],
        mode: "payment",
        success_url: "https://yoursite.com/success.html",
        cancel_url: "https://example.com/cancel",
    });

    res.json({ id: session.id });
});

router.post("/create/charge", async (req: Request, res: Response) => {
    try {
        const charge = await stripe.charges.create({
            amount: 2000,
            currency: 'usd',
            source: 'tok_amex',
            description: 'My First Test Charge (created for API docs)',
        });
        console.log(charge)
        res.status(200).send(charge)
    } catch (e) {
        console.log(e)
        res.status(500).send(e);
    }
})

export default router
