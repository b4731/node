import express, { Router, Response, Request } from "express"
import { PersonaControl } from '../controladores/Persona.control';
import { PersonaRequest } from '../dto/request/personaRequest';
import verificarAutenticacion from "../middleware/autenticacion/auth"
import auth from '../middleware/jwt-token';
import { UsuarioRequest } from "../dto/request/usuarioRequest";

let control = new PersonaControl()
let router = Router();


router.get("/listar", async (req: Request, res: Response) => {

    let result = await control.listarPersonas()
    res.status(200).json(result);
})

router.post("/registrar", async (req: Request, res: Response) => {
    try {
        let personaRequest = new PersonaRequest();
        personaRequest = req.body;
        let result = await control.registrar(personaRequest);
        res.status(200).send(result);
    } catch (error) {
        console.log(error)
        res.status(500).send(error);
    }

})

router.delete("/eliminar/:id", async (req: Request, res: Response) => {
    let { id } = req.params
    let idnumber = parseInt(id)
    let result = await control.eliminar(idnumber);
    res.send(result);
})
router.get("/buscar/:dni", async (req: Request, res: Response) => {
    try {
        let { dni } = req.params
        res.status(200).send(await control.buscarDni(dni));
    } catch (error) {
        console.log(error)
        res.status(500).send(error);

    }

})


router.put("/actualizar/:id", async (req: Request, res: Response) => {
    try {
        let { id } = req.params
        let idx = parseInt(id)
        let personaRequest = new PersonaRequest()
        personaRequest = req.body
        let filesReq = req.files?.listDni
        console.log('file', filesReq)
        if (filesReq != undefined) {
            let resultUploads = []
            // @ts-ignore
            for (const filesReqElement of filesReq) {
                console.log(filesReqElement)
                let files = filesReqElement
                //@ts-ignore
                files.mv('./uploads/manuales/' + files.name);
                //@ts-ignore
                let url = 'uploads/manuales/' + files.name
                resultUploads.push(url)
            }

            let urlString = ''
            console.log('arra', resultUploads)
            resultUploads.forEach(item => {
                urlString += item + '|'
            })

            let caracteres = urlString.length
            urlString = urlString.slice(0, caracteres - 1)
            console.log(urlString.length)
            //@ts-ignore
            personaRequest.archivoDni = urlString
        }

        let result = await control.actualizar(idx, personaRequest)
        res.status(200).send(result);

    } catch (error) {
        console.log(error)
        res.status(500).send(error);
    }
})

export default router
