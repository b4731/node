import express, { Router, Response, Request } from "express"
import verificarAutenticacion from "../middleware/autenticacion/auth"
import auth from '../middleware/jwt-token';
import { RolControl } from '../controladores/Rol.control';
import { RolRequest } from '../dto/request/rolRequest';

let control = new RolControl()
let router = Router();


router.get("/listar",  async (req: Request, res: Response) => {

    let result = await control.listar()
    res.status(200).json(result);
})

router.post("/registrar",  async (req: Request, res: Response) => {
    try {
        let personaRequest = new RolRequest();
        personaRequest = req.body;
        let result = await control.registrar(personaRequest);
        res.status(200).send(result);
    } catch (error) {
        console.log(error)
        res.status(500).send(error);
    }

})

router.delete("/eliminar/:id",  async (req: Request, res: Response) => {
    try {
        let { id } = req.params
        let idnumber = parseInt(id)
        let result = await control.eliminar(idnumber);
        res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error);
    }

})

export default router
