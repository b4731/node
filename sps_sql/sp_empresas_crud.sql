use pelucaCool;
DROP PROCEDURE IF EXISTS sp_empresas_crud;
DELIMITER $$

create procedure sp_empresas_crud
(
in xopcion int,
in xidx int,
in xruc varchar(11),
in xrazonSocial varchar(100),
in xdireccion varchar(150),
in xtelefono varchar(14),
in xcorreo varchar(100),
in xtipoNegocio int,
in xpropietarioId int

)
begin
declare ultimoInsertado int;
-- listar toda la informacion de la tabla empresas 
-- con sus propietarios y sacanso su informacion de la tabla persona

if xopcion=1 
	then
     select emp.id, emp.razonSocial,emp.ruc,emp.direccion,emp.telefono,emp.correo,emp.tipoNegocio,emp.propietarioId,
     pro.tipoComercial,personaId,per.nombre,per.apellido,per.fechaNacimiento,
     per.correo as personaCorreo,per.tipo,per.celular,per.dni,per.estado from empresa emp
     left join propietario pro on emp.propietarioId=pro.id
     left join persona per on pro.personaId=per.id
     order by emp.id asc
     ;
    end if;
    
 -- buscar por ruc   
if xopcion=2
	then
     select *from empresa emp where emp.ruc=xruc;
    end if;
-- buscarpor ID
if xopcion=3 then 
	select *from empresa emp where emp.id=xidx;
end if;
-- eliminar por id
if xopcion=4 then 
	delete from empresa where id=xidx;
end if;
    
    if xopcion=5
	then
     insert into empresa(razonSocial,ruc,direccion,telefono,correo,propietarioId)
     values(xrazonSocial,xruc,xdireccion,xtelefono,xcorreo,xpropietarioId);
     set ultimoInsertado =last_insert_id();
     select *from empresa empr where empr.id=ultimoInsertado;
     
    end if;
    
    if xopcion=6
	then
     update empresa set razonSocial=xrazonSocial,
     ruc=xruc,
     direccion=xdireccion,
     telefono=xtelefono,
     correo=xcorreo,
     propietarioId=xpropietarioId
     where id=xidx;
    
     
    end if;
end $$
delimiter $$


