use pelucaCool;
drop procedure if exists sp_propietario_crud;
DELIMITER $$

create procedure sp_propietario_crud(
    in xopcion int ,
    in xidpersona int,
    in xidpropietario int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xtipoComercial varchar(100)
    )
begin
    declare ultimoRegistrado int;
    declare propietarioIdinsert int;
    -- actualizar informacion de propietarios
    if xopcion=1 then
        update propietario pro set pro.tipoComercial=xtipoComercial
        where pro.id=xidpropietario;

        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
                              pe.correo=xcorreo,
                              pe.celular=xtelefono,
                              pe.dni=xdni,
                              pe.tipo=xtipoPersona,
                              pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into propietario(personaId)values (ultimoRegistrado);
        set propietarioIdinsert=last_insert_id ();
        select pro.id,pro.tipoComercial,json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,
                       'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona
        from propietario pro
            left join persona p on p.id = pro.personaId
        where pro.id=propietarioIdinsert;

    end if;

    if xopcion=3 then
        delete from propietario where id=xidpropietario;
        delete from persona where id=xidpersona;
    end if;

end $$
delimiter $$
