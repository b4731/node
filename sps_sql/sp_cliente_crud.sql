use pelucaCool;
drop procedure if exists sp_cliente_crud;
DELIMITER $$

create procedure sp_cliente_crud(
    in xopcion int ,
    in xidpersona int,
    in xidcliente int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xestado int,
    in xusuario varchar(100),
    in xclave varchar(100),
    in xrol int
)
begin
    declare ultimoRegistrado int;
    declare Idinsert int;
    -- buscar por id
    if xopcion=1 then
        select c.idCliente,c.estado,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from cliente c left join persona p2 on p2.id = c.personaId

        where c.idCliente=xidcliente;
    end if;

    -- insertar cliente
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into cliente(personaId)values (ultimoRegistrado);
        set Idinsert=last_insert_id ();
        insert into usuario( usuario, clave,  rolIdRol, personaId)
        values (xusuario,xclave,xrol,ultimoRegistrado);


        select c.idCliente,
               json_object(
                   'id',p.id,
                   'nombre',p.nombre,
                   'apellido',p.apellido,
                   'fechaNacimiento',p.fechaNacimiento,
                   'tipo',p.tipo,
                   'celular',p.celular,
                   'dni',p.dni,
                   'correo',p.correo,
                   'estado',p.estado,
                   'createdAt',p.createdAt) as persona,
        json_object('idUsuario',u.idUsuario,'clave',u.clave,'idRol',u.rolIdRol,'usuario',u.usuario) as usuario
        from cliente c
                 left join persona p on p.id = c.personaId
        left join usuario u on p.id = u.personaId
        where c.idCliente=Idinsert;

    end if;

    -- eliminar
    if xopcion=3 then
        delete from cliente where idCliente=xidcliente;
        delete from persona where id=xidpersona;
    end if;

    if xopcion=4 then
        select c.idCliente,c.estado,c.personaId,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from cliente c left join persona p2 on p2.id = c.personaId;

    end if;

    -- actualizacion de emplead
    if xopcion=5 then
        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
                              pe.correo=xcorreo,
                              pe.celular=xtelefono,
                              pe.dni=xdni,
                              pe.tipo=xtipoPersona,
                              pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
end $$
delimiter $$
