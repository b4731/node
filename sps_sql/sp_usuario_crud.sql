use pelucaCool;
drop procedure if exists sp_usuario_crud;
DELIMITER $$

create procedure sp_usuario_crud(
in xopcion int ,
in xid int,
in xusuario varchar(150),
in xclave varchar(150))
begin
    declare ultimoRegistrado int;
    -- lista de usuarios
if xopcion=1 then
    select
           u.idUsuario,
        u.usuario ,
        u.clave,
        json_object(
                'id', p.id,
                'nombre', p.nombre,
                'apellido', p.apellido,
                'celular', p.celular,
                'correo', p.correo,
                'dni', p.dni,
                'fechaNacimiento', p.fechaNacimiento,
                'tipo', p.tipo,
                'estado', p.estado)
              as persona,

        json_object(
                'nombre',r.nombre,
                'idRol',r.idRol
            ) as rol

    from usuario u left join persona p on p.id = u.personaId
                   left join rol r on r.idRol = u.rolIdRol;
end if;

    -- busqueda por id
    if xopcion=2 then
        select u.idusuario, usuario, clave, rolidrol, personaid,
               json_object('nombre',r.nombre,'idRol',r.idRol,'estado',r.estado) as rol,
               json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,
                   'estado',p.estado,'fechaNacimiento',p.fechaNacimiento,
                   'dni',p.dni,'correo',p.correo,'celular',p.celular,
                   'tipo',p.tipo) as persona
        from usuario u
        left join rol r on r.idRol = u.rolIdRol
        left join persona p on p.id = u.personaId
        where u.idUsuario=xid;

        end if;

    -- busqueda por clave y usuario
    if xopcion=3 then
        select u.idusuario, usuario, clave, rolidrol, personaid,
               json_object('nombre',r.nombre,'idRol',r.idRol,'estado',r.estado) as rol
        from usuario u                                                                                                left join rol r on r.idRol = u.rolIdRol
        where u.usuario=xusuario and u.clave=xclave;

    end if;
end $$
delimiter $$

call sp_usuario_crud(2,1,null,null);

