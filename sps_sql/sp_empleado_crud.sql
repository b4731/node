use pelucaCool;
drop procedure if exists sp_empleado_crud;
DELIMITER $$

create procedure sp_empleado_crud(
    in xopcion int ,
    in xidpersona int,
    in xidEmpleado int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xtipoEmpleado varchar(100),
    in xEmpresaId int
)
begin
    declare ultimoRegistrado int;
    declare propietarioIdinsert int;
    -- buscar por id
    if xopcion=1 then
        select em.idEmpleado,em.areaId,em.empresaId,em.personaId,em.tipoEmpleado,
               JSON_OBJECT('id',e.id,'correo',e.correo,'direccion',e.direccion,
                           'razonSocial',e.razonSocial,'ruc',e.ruc,'telefono',e.telefono,
                           'tipoNegocio',e.tipoNegocio,'propietarioId',e.propietarioId) AS empresa,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from empleado em left join persona p2 on p2.id = em.personaId
                         left join empresa e on e.id = em.empresaId
        where em.idEmpleado=xidEmpleado;
    end if;

    -- insertar empleado
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into empleado(personaId,tipoEmpleado,empresaId)values (ultimoRegistrado,xtipoEmpleado,xEmpresaId);
        set propietarioIdinsert=last_insert_id ();
        select pro.idEmpleado,pro.tipoEmpleado,json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,
                                                    'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona
        from empleado pro
                 left join persona p on p.id = pro.personaId
        where pro.idEmpleado=propietarioIdinsert;

    end if;

    -- eliminar
    if xopcion=3 then
        delete from empleado where idEmpleado=xidEmpleado;
        delete from persona where id=xidpersona;
    end if;

    if xopcion=4 then
        select em.idEmpleado,em.areaId,em.empresaId,em.personaId,em.tipoEmpleado,
               JSON_OBJECT('id',e.id,'correo',e.correo,'direccion',e.direccion,
                   'razonSocial',e.razonSocial,'ruc',e.ruc,'telefono',e.telefono,
                   'tipoNegocio',e.tipoNegocio,'propietarioId',e.propietarioId) AS empresa,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                   'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                   'estado',p2.estado,'tipo',p2.tipo) as persona
        from empleado em left join persona p2 on p2.id = em.personaId
        left join empresa e on e.id = em.empresaId;
    end if;

    -- actualizacion de emplead
    if xopcion=5 then
        update empleado em set  em.tipoEmpleado=xtipoEmpleado,em.empresaId=xEmpresaId
        where em.idEmpleado=xidEmpleado;
        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
            pe.correo=xcorreo,
            pe.celular=xtelefono,
            pe.dni=xdni,
            pe.tipo=xtipoPersona,
            pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
end $$
delimiter $$
